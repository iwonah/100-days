# Twoim zadaniem jest napisanie kodu, który:
# 1.Pobierze plik https://zerotojunior.dev/cezar.txt.
# 2.Odszyfruje go (i automatycznie wykryje, że plik jest odszyfrowany).
# 3.Wypisze odszyfrowany tekst na ekranie.

# Zawartość tekstu jest zaszyfrowana szyfrem Cezara, z użyciem dwóch alfabetów:
# small = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż"
# large = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ"
# Po odszyfrowaniu policz wystąpienie wszystkich liter. 
# Jeśli najwięcej jest liter "e", istnieje wysokie prawdopodobieństwo, że odszyfrowanie udało się poprawnie :)
# Jeśli tak nie jest, zwiększ przesunięcie o 1 i rozpocznij od nowa.

import os
import requests

url = "https://zerotojunior.dev/cezar.txt"

response = requests.get(url)

if response.status_code == 200:
    text = response.content.decode('UTF-8', errors='ignore')
    print("Pobrany tekst:")
    print(text)
else:
    print(f"Błąd pobierania pliku: {response.status_code}")

def decrypt_caesar_cipher(text, shift):
    lowercase_alphabet = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż"
    uppercase_alphabet = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ"
    decrypted_text = ""

    for letter in text:
        if letter in lowercase_alphabet:
            index = (lowercase_alphabet.index(letter) - shift) % len(lowercase_alphabet)
            decrypted_text += lowercase_alphabet[index]
        elif letter in uppercase_alphabet:
            index = (uppercase_alphabet.index(letter) - shift) % len(uppercase_alphabet)
            decrypted_text += uppercase_alphabet[index]
        else:
            decrypted_text += letter

    return decrypted_text

	
def most_frequent_letter(text):
    text = text.lower()
    letter_counts = {}
    alphabet = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż"

    for letter in text:
        if letter in alphabet:
            if letter in letter_counts:
                letter_counts[letter] += 1
            else:
                letter_counts[letter] = 1

    most_frequent = max(letter_counts, key=letter_counts.get)
    return most_frequent


def final_translation():
    for shift in range(1,31):
        if most_frequent_letter(decrypt_caesar_cipher(text, shift)) == "e":
            return decrypt_caesar_cipher(text, shift)

print(final_translation())
