# Twoim zadaniem jest stworzenie WŁASNEJ funkcji substring. Nie używaj systemowej, tylko napisz ją od zera (nie używaj slice(), ani [x:y])

def substring(a,b,text)
    """Returns a substring of text starting with index a, ending with index b"""
    substring=""
    for ind in range(a,b)
        substring += text[ind]
    return substring