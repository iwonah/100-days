# Przygotuj prosty słownik (dosłownie) polsko - angielski. 10-20 wyrazów max.
# Następnie komputer ma poprosić o wpisanie przez Ciebie zdania. 
# Po jego wprowadzeniu ma przetłumaczyć wszystkie wyrazy, które znajdzie w słowniku i wydrukować przetworzone zdanie na ekranie.
# Wyrazy, które odnalazł, mają być przetłumaczone. 
# Wyrazy, których nie odnalazł w słowniku. mają być wypisane, tak jak zostały wprowadzone, ale dodatkowo z * na końcu.
# Zadbaj o to, żeby pierwsza litera w zdaniu była pisana dużą literą.


pl_eng_dictionary = {"mam":"have", "ja":"I", "jestem":"am", "mogę":"can", "jeszcze":"more", "wiem": "know", "to":"it", "muszę": "have to", "ale": "but", "dlaczego": "why", "tam": "there", "kto": "who",}

sentence = input("Wrpowadź zdanie po polsku do przetłumaczenia na angielski:")
sentence_in_words = sentence.split()
sentence_translation = []

for word in sentence_in_words:
    if word in pl_eng_dictionary:
        sentence_translation.append(pl_eng_dictionary[word])
    else:
        sentence_translation.append(word + "*")
        
sentence_translation_str = " ".join(sentence_translation)
print(sentence_translation_str.capitalize())