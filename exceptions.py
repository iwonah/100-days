# Zadanie na dzisiaj:

# Napisz funkcję divide(a, b), która dzieli dwie liczby. 
# Funkcja powinna zgłaszać wyjątki w następujących sytuacjach:
# 1.	Jeśli a lub b nie są liczbami, zgłoś TypeError.
# 2.	Jeśli b wynosi 0, zgłoś ZeroDivisionError.
# 3.	Jeśli a lub b są ujemne, zgłoś własny wyjątek NegativeNumberException.
# Następnie użyj bloku try...except, aby przechwycić i obsłuży każdy z tych wyjątków indywidualnie.

class NegativeNumberException(Exception):
    pass

def divide(a,b):
    try:
        a / b
        if a < 0 or b < 0:
            raise NegativeNumberException("You can not operate on negative numbers!")
        return a / b
    except (TypeError, ZeroDivisionError, NegativeNumberException) as e:
        return f"Error: {e}"

print(divide(1,-2))




