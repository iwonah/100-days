# Stwórz generator hasła.
# 1.	 Hasło musi zawierać co najmniej jedną cyfrę.
# 2.	 Hasło musi zawierać co najmniej jedną dużą literę.
# 3.	 Hasło musi zawierać co najmniej jedną małą literę.
# 4.	 Hasło musi zawierać co najmniej jeden znak specjalny.
# 5.	 Hasło musi mieć między 10 a 15 znaków.
# 6.	 Hasło nie może zawierać znaków "mylących", 1, I, O, 0.


import string

uppercase_alphabet = list(string.ascii_uppercase)
uppercase_alphabet.remove("O")

lowercase_alphabet = list(string.ascii_lowercase)
lowercase_alphabet.remove("l")

digits = list(range(2,10))

special_signs = list(string.punctuation)
special_signs.remove("\\")

import random

def generate_password():
    password_list = []
    password_list.append(random.choice(uppercase_alphabet))
    password_list.append(random.choice(lowercase_alphabet))
    password_list.append(random.choice(digits))
    password_list.append(random.choice(special_signs))
    while random.randint(10,15) >= len(password_list):
        comepletely_random = [random.choice(lowercase_alphabet), random.choice(uppercase_alphabet), random.choice(digits), random.choice(special_signs)]
        a = random.choice(comepletely_random)
        password_list.append(a)
    random.shuffle(password_list)
    password = "".join(str(i) for i in password_list)
    return password

print(generate_password())











# print(password)