# Napisz funkcję, która przyjmuje listę liczb całkowitych i zwraca listę zawierającą tylko unikalne wartości z tej listy.

my_list = [1, 2, 6, 3, 3, 4, 5, 6, 7, 8, 9, 10]

def get_unique_values(a_list): return list(set(a_list))

print(get_unique_values(my_list))