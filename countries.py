countries = {"Polska": "Warszawa",
             "Niemcy": "Berlin",
             "Francja": "Paryż",
             "Włochy": "Rzym"}

country = input("Wpisz nazwę pańtwa, by poznać jego stolicę: \n")

while True:
    if country in countries:
        print(countries[country])
    elif country == "exit":
        break    
    else:
        print("To nie jest Państwo, bądź nie ma go na mojej liście.")
    country = input("Wpisz nazwę pańtwa, by poznać jego stolicę: \n")

