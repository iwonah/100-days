# Napisz program, który losuje 5 liczb całkowitych z zakresu od 1 do 10,
# a następnie prosi użytkownika o odgadnięcie co najmniej jednej z tych liczb. 
# Program powinien odpowiedzieć, czy podana przez użytkownika liczba była jedną z wylosowanych lub nie.

import random

num_list = random.sample(range(1,11), 5)

def guessing_game():
    print("Welcome to the guessing game.")
    print("Choose a number from 1 to 10")
    while True:
        guess = input("Enter your number: (To exit the game, just press 'Enter')\n")
        if guess =="":
            return False
        elif int(guess) in num_list:
            print(f"Congratulations, my numbers were:{num_list} and you've guessed one of them!")
        else:
            print(f"I'm sorry, but you're gonna have to try again. My numbers were: {num_list} and your guess is not among them.")

guessing_game()