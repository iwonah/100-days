from datetime import datetime

date_1 = datetime.now()
date_2 = datetime(1994, 3, 22)

diff = date_1 - date_2

print(f"I'm ancient, I lived for {diff.days} days and counting!")