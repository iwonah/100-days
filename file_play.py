# Napisz program w Pythonie, który zorganizuje pliki tekstowe z folderu "źródłowego" 
# do folderów "docelowych" na podstawie ich rozszerzeń. 
# Przykładowo, wszystkie pliki .txt powinny zostać przeniesione do folderu txt, a pliki .csv do folderu csv.
# Kroki do wykonania zadania:
# 1.	Utwórz folder źródłowy source oraz kilka plików tekstowych z różnymi rozszerzeniami, takimi jak .txt, .csv i .md.
# 2.	Napisz program, który:
# a. Wyszuka wszystkie pliki tekstowe w folderze źródłowym.
# b. Dla każdego pliku utworzy folder docelowy (jeśli jeszcze nie istnieje) na podstawie rozszerzenia pliku.
# c. Przeniesie plik do odpowiedniego folderu docelowego.


import glob
import os
import shutil

def sort_files(file_extention,folder_name):

    files = glob.glob(file_extention)

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
        print(f"Utworzono folder {folder_name}")

    for file in files:
        shutil.move(file, folder_name)
        print(f"Przeniesiono {file} do {folder_name}")

sort_files("*.txt", "Notatki")
sort_files("*.md", "Kody")
sort_files("*.csv", "Arkusze")