# Lista zakupów, które trzeba kupić:
# jajka, mleko, chleb, jajka, masło, jajka, jabłka, chleb

# Przygotuj aplikację która.
# 1.	Będzie miała wbudowany cennik wszystkich produktów.
# 2.	Przygotuje listę zakupów, złożoną z unikalnych produktów oraz podanej obok liczby produktów do kupienia.
# 3.	Wyliczy wartość zakupów i poda ją w podsumowaniu


prices = {"eggs":1, "milk":3, "bread":4, "butter":6}
shopping_list = set()
shopping_list_with_quantities = {}
total_cost = []

def get_shopping_list():
    while True:
        item = input("What groceries do you need to get? (To finish the shopping list, just press enter):\n")
        if item == "":
            break
        else:
            shopping_list.add(item)

def get_quantities():
    for item in shopping_list:
        quantity = int(input(f"How much/many {item} do you need?\n"))
        shopping_list_with_quantities[item] = quantity

def print_shopping_list():
    print("Your shopping list:")
    for item in shopping_list_with_quantities:
        print(item, shopping_list_with_quantities[item])

def print_total_cost():
    for item in shopping_list_with_quantities:
        if item in prices:
            total_cost.append(prices[item] * shopping_list_with_quantities[item])
        else:
            continue
    print(f"Total cost of the groceries I have the prices of is: {sum(total_cost)}zł")

def make_a_shopping_list():
    get_shopping_list()
    get_quantities()
    print_shopping_list()
    print_total_cost()

make_a_shopping_list()
